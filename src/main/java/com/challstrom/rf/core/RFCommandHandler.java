package com.challstrom.rf.core;

import com.challstrom.rf.core.packet.RFPacket;

import java.net.DatagramPacket;

public abstract class RFCommandHandler {
    public abstract void handleCommand(RFPacket rfPacket, DatagramPacket datagramPacket);
}
