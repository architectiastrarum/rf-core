package com.challstrom.rf.core;

import java.net.InetAddress;

public class RFRoute {
    private final InetAddress address;
    private final int port;

    public RFRoute(InetAddress address, int port) {
        this.address = address;
        this.port = port;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RFRoute)) return false;

        RFRoute rfRoute = (RFRoute) o;

        return getPort() == rfRoute.getPort() && getAddress().equals(rfRoute.getAddress());
    }

    @Override
    public int hashCode() {
        int result = getAddress().hashCode();
        result = 31 * result + getPort();
        return result;
    }
}
