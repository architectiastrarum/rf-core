package com.challstrom.rf.core.packet;

import com.challstrom.rf.core.RFRoute;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class RFPacketSender implements Runnable {
    private final RFPacketBuffer rFPacketBuffer;
    private final DatagramSocket socket;
    private final AtomicBoolean running;
    private final int delay;

    private ConcurrentLinkedQueue<RFRoute> routes;

    public RFPacketSender(RFPacketBuffer rFPacketBuffer, DatagramSocket socket, AtomicBoolean running, int delay, ConcurrentLinkedQueue<RFRoute> routes) {
        this.rFPacketBuffer = rFPacketBuffer;
        this.socket = socket;
        this.running = running;
        this.delay = delay;
        this.routes = routes;
    }

    @Override
    public void run() {
        while (running.get()) {
            if (rFPacketBuffer.hasNext()) {
                RFPacket rfPacket = rFPacketBuffer.removeNextPacket();
                routes.parallelStream().forEach(rfRoute -> {
                    try {
                        DatagramPacket datagramPacket = RFPacketConverter.rfPacketToDatagram(rfPacket, rfRoute);
                        if (RFVerbose.useVerbose) {
                            System.out.println("Sending RFPacket with length " + datagramPacket.getData().length + " bytes.");
                        }
                        if (datagramPacket.getData().length > 1024) System.err.println("RFPacket is too large to send! Server buffer is 1024 bytes!");
                        socket.send(datagramPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        socket.close();
    }
}
