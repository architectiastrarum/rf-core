package com.challstrom.rf.core.packet;

import com.challstrom.rf.core.RFSec;

import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.zip.CRC32;

public class RFPacketFactory {

    public RFPacket getDataPacket(String unEncodedMessage, String passphrase) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKey secretKey = RFSec.getSecret(passphrase);
        byte[][] ivAndCipherText = RFSec.encrypt(unEncodedMessage, secretKey);
        byte[] iv = ivAndCipherText[0];
        byte[] cipherText = ivAndCipherText[1];
        final CRC32 crc32 = new CRC32();
        crc32.update(unEncodedMessage.getBytes());
        long crc = crc32.getValue();

        return new RFPacket(iv, cipherText, crc, false);
    }

    public RFPacket getCommand(String command) {
        return new RFPacket(null, command.getBytes(), 0, true);
    }
}
