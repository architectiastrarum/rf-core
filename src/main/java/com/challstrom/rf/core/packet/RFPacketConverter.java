package com.challstrom.rf.core.packet;

import com.challstrom.rf.core.RFRoute;

import java.io.*;
import java.net.DatagramPacket;

public class RFPacketConverter {


    public static DatagramPacket rfPacketToDatagram(RFPacket rfPacket, RFRoute rfRoute) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(rfPacket);
        return new DatagramPacket(byteArrayOutputStream.toByteArray(), byteArrayOutputStream.size(), rfRoute.getAddress(), rfRoute.getPort());
    }

    public static RFPacket datagramToRfPacket(DatagramPacket datagramPacket) throws IOException, ClassNotFoundException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(datagramPacket.getData());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        //let's hope this doesn't blow up
        return (RFPacket) objectInputStream.readObject();
    }
}
