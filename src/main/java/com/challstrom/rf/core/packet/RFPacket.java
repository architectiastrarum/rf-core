package com.challstrom.rf.core.packet;

import java.io.Serializable;
import java.util.Arrays;

public class RFPacket implements Serializable {
    private final byte isCommand;
    private final byte[] iv;
    private final byte[] encodedMessage;
    private final long crc;


    RFPacket(byte[] iv, byte[] encodedMessage, long crc, boolean isCommand) {
        this.isCommand = isCommand ? (byte) 0xF : (byte) 0x0;
        this.iv = iv;
        this.encodedMessage = encodedMessage;
        this.crc = crc;
    }

    public byte[] getIv() {
        return iv;
    }

    public byte[] getEncodedMessage() {
        return encodedMessage;
    }

    public long getCrc() {
        return crc;
    }

    public boolean isCommand() {
        return isCommand == (byte) 0xF;
    }

    @Override
    public String toString() {
        return "RFPacket{" +
                "isCommand=" + isCommand +
                ", iv=" + Arrays.toString(iv) +
                ", encodedMessage=" + new String(encodedMessage) +
                ", crc=" + crc +
                '}';
    }
}
