package com.challstrom.rf.core.packet;

import java.util.concurrent.ConcurrentLinkedQueue;

public class RFPacketBuffer {
    private ConcurrentLinkedQueue<RFPacket> RFPackets;

    public RFPacketBuffer() {
        RFPackets = new ConcurrentLinkedQueue<RFPacket>();
    }

    public boolean hasNext() {
        return RFPackets.size() > 0;
    }

    public RFPacket removeNextPacket() {
        return RFPackets.remove();
    }

    public void addPacket(RFPacket RFPacket) {
        RFPackets.add(RFPacket);
    }


}
