package com.challstrom.rf.core.packet;

import com.challstrom.rf.core.RFCommandHandler;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.atomic.AtomicBoolean;

public class RFPacketReceiver implements Runnable {

    private final RFPacketBuffer rFPacketBuffer;
    private final DatagramSocket socket;
    private final AtomicBoolean running;
    private final RFCommandHandler commandHandler;

    public RFPacketReceiver(RFPacketBuffer RFPacketBuffer, DatagramSocket socket, AtomicBoolean running, RFCommandHandler commandHandler) {
        this.rFPacketBuffer = RFPacketBuffer;
        this.socket = socket;
        this.running = running;
        this.commandHandler = commandHandler;
    }

    public void run() {
        while (running.get()) {
            byte[] buffer = new byte[1024];//512 = 330 characters & 1024 = 842 MAX
            DatagramPacket receivedDatagram = new DatagramPacket(buffer, buffer.length);
            try {
                socket.receive(receivedDatagram);
            } catch (IOException e) {
                e.printStackTrace();
            }
            RFPacket receivedPacket;
            try {
                receivedPacket = RFPacketConverter.datagramToRfPacket(receivedDatagram);
                if (RFVerbose.useVerbose) {
                    synchronized (socket) {
                        System.out.println("=====Packet Received=====");
                        System.out.println(receivedPacket);
                        System.out.println("=========================");
                    }
                }
                if (receivedPacket.isCommand()) {
                    commandHandler.handleCommand(receivedPacket, receivedDatagram);
                } else {
                    rFPacketBuffer.addPacket(receivedPacket);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        socket.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

    }
}
