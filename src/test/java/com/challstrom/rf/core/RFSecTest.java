package com.challstrom.rf.core;

import org.junit.Test;

import javax.crypto.SecretKey;
import java.util.zip.CRC32;

import static org.junit.Assert.assertEquals;

public class RFSecTest {

    @Test
    public void encryptDecrypt() throws Exception {
        String plainText = "Foobar";
        CRC32 crc32 = new CRC32();
        crc32.update(plainText.getBytes());
        long crc1 = crc32.getValue();
        crc32.reset();
        SecretKey secretKey1 = RFSec.getSecret("hello");
        byte[][] ivAndCipherText = RFSec.encrypt(plainText, secretKey1);
        SecretKey secretKey2 = RFSec.getSecret("hello");
        String decrypted = RFSec.decrypt(ivAndCipherText[1], ivAndCipherText[0], secretKey2);
        crc32.update(decrypted.getBytes());
        long crc2 = crc32.getValue();
        assertEquals(plainText, decrypted);
        assertEquals(crc1, crc2);
    }

}